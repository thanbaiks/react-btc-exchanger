import { ActionTypes } from './actions';
import { put, takeLatest, call } from 'redux-saga/effects';
import { delay } from 'redux-saga';

import * as utils from './utils';
import actions from './actions';

function* fetchData(action) {
    try {
        const data = yield call(utils.fetchBTCRates);
        yield put(actions.arrived(data));
    } catch (e) {
        yield put(actions.error(e.message));
    }
}

export default function* () {
    yield takeLatest(ActionTypes.data.pending, fetchData);
}
