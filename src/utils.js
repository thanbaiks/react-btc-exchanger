import actions from './actions';

export function fetchBTCRates() {
    return fetch('https://api.coindesk.com/v1/bpi/currentprice.json', {cache: 'no-store'})
        // Mock error
        // .then(() => Promise.reject({
        //     message: "Oops! Should display this error!"
        // }))
        .then(resp => resp.json())
        .then(json => Object.values(json.bpi));
}