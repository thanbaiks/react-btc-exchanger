import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import Actions from './actions';
import { Container, Button } from './styles';
import exchangeImg from './assets/exchange.png';

const Wrapper = Container.extend`
    width: 640px;
    height: 480px;
`;

const InnerWrapper = styled.div`
    width: 100%;
    flex: 1;
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const Side = styled.div`
    flex: 1;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

const Center = Side.extend`
    flex: 0;
    justify-content: space-around;
`;

const ImgExchange = styled.img`
    width: 240px;
`;

const Heading = styled.h1`
    font-size: 18pt;
    font-weight: bold;
`;

const Currency = (()=>{
    const Wrapper = styled.div`
        background: #FFFFFF;
        border: solid 2px #4facfe;
        border-radius: 4px;
        height: 64px;
        width: 128px;
        margin: 8px 0;
        padding: 4px;
        transition: all ease 200ms;
        &:hover {
            background: #4facfe;
            color: #FFFFFF;
        }
    `;

    const Big = styled.h3`
        margin: 0;
    `;

    const Small = styled.h4`
        margin: 0;
    `;

    const InputField = styled.input`
        border: none;
        outline: none;
        font-size: 16pt;
        text-align: center;
        background: none;
        width: 80px;
        border-bottom: solid 1px #fa709a;
        font-family: 'Encode sans', sans-serif;
    `;

    const Flexbox = styled.div`
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    `;

    const rounded = (n, t) => {
        return Math.round(n*(10**t))/(10**t);
    };

    const Currency = class extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                editing: false
            };
        }

        onChange = (e) => {
            const {target: { value } } = e;
            const { dispatch, rate_float: rate } = this.props;
            dispatch(Actions.change(parseFloat(value)/rate));
        }

        toggleEdit = (e) => {
            this.setState(prev => ({ editing: !prev.editing }));
        }

        mountInput = (el) => {
            this.input = el;
            if (!el) return;
            el.value = rounded(this.props.value, 2);
            el.oninput = () => {
                if (isFinite(parseFloat(el.value))) {
                    this.props.dispatch(Actions.change(parseFloat(el.value)/this.props.rate_float));
                }
            };
        }

        componentWillUpdate({value}) {
            this.input && (this.input.value = rounded(value, 2));
        }

        render(){
            const { value, code, rate_float: rate, description } = this.props;
            const { editing } = this.state;

            return (
                <Wrapper title={rate}>
                    {!editing ?
                    <Flexbox>
                        <Big onClick={this.toggleEdit}>{rounded(value, 2)}</Big>
                        <Small>{code}</Small>
                    </Flexbox> : 
                    <Flexbox>
                        <InputField innerRef={ el => this.mountInput(el) } onBlur={this.toggleEdit} />
                    </Flexbox>}
                </Wrapper>
            );
        }
    }

    return connect()(Currency);
})();

const InputField = styled.input`
    border: none;
    outline: none;
    font-size: 22pt;
    text-align: center;

    background: none;
    width: 120px;
    border-bottom: solid 1px #fa709a;
    font-family: 'Encode sans', sans-serif;
`;

const Exchange = ({ rates, lastUpdate, input, onInputChanged, onFetchRequested, trans }) => (
    <Wrapper style={{opacity: trans.x, transform: `translateY(-${trans.x*50}px)`}}>
        <Heading>BTC exchanger</Heading>
        <InnerWrapper>
            <Side>
                <InputField value={input} onChange={e => onInputChanged(e.target.value)}/>
                <h3 style={{ margin: '8px 0' }}>BTC</h3>
            </Side>
            <Center>
                <ImgExchange src={exchangeImg} />
                <Button onClick={onFetchRequested}>Reload</Button>
            </Center>
            <Side>
                {rates.map(unit => <Currency key={unit.code} {...unit} value={input*unit.rate_float}/>)}
            </Side>
        </InnerWrapper>
        <h5 style={{margin: '8px 0', color: '#AAAAAA'}}>Last update at: {lastUpdate.toLocaleString()}</h5>
    </Wrapper>
);

const mapStateToProps = (state) => ({
    rates: state.rates,
    input: state.input,
    lastUpdate: state.lastUpdate
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    onInputChanged(input) {
        dispatch(Actions.change(input));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Exchange);
