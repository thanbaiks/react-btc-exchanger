import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Loader from 'halogen/RotateLoader';
import { Motion, spring } from 'react-motion';

import Exchanger from './Exchanger';
import ErrorDialog from './ErrorDialog';
import Actions from './actions';

const Wrapper = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const App = class extends Component {
    fetch = () => {
        const dispatch = this.props.dispatch;
        dispatch(Actions.pending());
    }

    componentWillMount() {
        this.fetch();
    }

    render() {
        const { pending, error, onRetry } = this.props;
        return (
            <Wrapper>
            {
                pending ? 
                    <Loader/>
                : error ?
                    <ErrorDialog error={error} onRetry={this.fetch} />
                :
                    <Motion defaultStyle={{x: 0}} style={{x: spring(1)}}>
                        {style => <Exchanger onFetchRequested={this.fetch} trans={style}/>}
                    </Motion>
            }
            </Wrapper>
        );
    };
}

const mapStateToProps = (state) => ({
  pending: state.rates.length === 0 && !state.error,
  error: state.error,
});

export default connect(mapStateToProps)(App);
