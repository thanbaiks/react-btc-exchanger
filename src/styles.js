import styled from 'styled-components';

export const Container = styled.div`
    width: 400px;
    height: 320px;
    border-radius: 4px;
    box-shadow: 0 0 8px #FA709A;
    background: #FFFFFF;
    text-align: center;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-around;
`;


export const Button = styled.button`
    width: 140px;
    height: 48px;
    border: none;
    outline: none;
    border-radius: 4px;
    color: #FFFFFF;
    background-image: linear-gradient(120deg, #89f7fe 0%, #66a6ff 100%);
    box-shadow: 0 0 8px #95CADC;
    cursor: pointer;
    font: 'Encode Sans', sans-serif;
    font-size: 13pt;
    text-shadow: 0 0 3px rgba(0,0,0,.5);
`;