import styled from 'styled-components';
import React from 'react';
import img from './assets/sad.png';
import { Container, Button } from './styles';

const Wrapper = Container.extend`
    width: 480px;
`;

const Img = styled.img`
    width: 200px;
`;

const Message = styled.span`
    font-size: 16pt;
    color: #888888;
`;


export default ({error, onRetry}) => (
    <Wrapper>
        <Img src={img} />
        <Message>{error}</Message>
        <Button onClick={onRetry}>Retry</Button>
    </Wrapper>
);
