import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

export default function (reducer) {
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [
        sagaMiddleware
    ];
    const enhancers = [
        applyMiddleware(...middlewares),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ].filter(Boolean);
    return {
        ...createStore(reducer, compose(...enhancers)),
        runSaga: sagaMiddleware.run
    };
}
